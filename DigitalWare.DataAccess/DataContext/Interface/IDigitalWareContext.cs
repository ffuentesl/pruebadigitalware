using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Usuario;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Interface
{
    public interface IDigitalWareContext
    {
        DbSet<Cliente> Clientes { get; set; }
        DbSet<Producto> Productos { get; set; }
        DbSet<ExistenciaProducto> ExistenciaProductos { get; set; }
        DbSet<Venta> Ventas { get; set; }
        DbSet<DetalleVentaProducto> DetalleVentaProductos { get; set; }
    }
}