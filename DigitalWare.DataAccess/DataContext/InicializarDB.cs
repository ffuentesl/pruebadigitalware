namespace DigitalWare.DataAccess.DataContext
{
    public class InicializarDB
    { 
        public static void Inicializar(DigitalWareContext context)
        {
            context.Database.EnsureCreated();
        }
    }

}