using System;
using DigitalWare.DataAccess.DataContext.Interface;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;
using DigitalWare.DataAccess.Entity.Usuario;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext
{
    public class DigitalWareContext : DbContext, IDigitalWareContext
    {
        public DigitalWareContext(DbContextOptions<DigitalWareContext> options)
            : base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<ExistenciaProducto> ExistenciaProductos { get; set; }
        public DbSet<Venta> Ventas { get; set; }
        public DbSet<DetalleVentaProducto> DetalleVentaProductos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //configurar la propiedad Identificacion para no tener valores duplicados    
            modelBuilder.Entity<Cliente>()
                .HasIndex(k => k.Identificacion)
                .IsUnique();

          //  modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);

        }
    }


    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>().HasData(
                new Cliente()
                {
                     Nombre = "Alejandra", Apellido = "Sarmiento",
                    FechaNacimiento = new DateTime(1970, 02, 11), Identificacion = 1020378,
                    TelefonoCelularFijo = "6378891"
                },
                new Cliente()
                {
                     Nombre = "Marcela", Apellido = "Avellaneda",
                    FechaNacimiento = new DateTime(1989, 12, 21), Identificacion = 1000308,
                    TelefonoCelularFijo = "2478391"
                },
                new Cliente()
                {
                     Nombre = "Andrea", Apellido = "Lopez",
                    FechaNacimiento = new DateTime(1994, 12, 11), Identificacion = 10923, TelefonoCelularFijo = "637833"
                },
                new Cliente()
                {
                     Nombre = "Luisa", Apellido = "Martenez",
                    FechaNacimiento = new DateTime(1988, 04, 18), Identificacion = 773374,
                    TelefonoCelularFijo = "4370003"
                },
                new Cliente()
                {
                    Nombre = "Viviana", Apellido = "Prada",
                    FechaNacimiento = new DateTime(1988, 04, 18), Identificacion = 7745374,
                    TelefonoCelularFijo = "43700903"
                }
            );

            /*modelBuilder.Entity<Producto>().HasData(
                new Producto()
                {
                    Id = 0,
                    Nombre = "Chocolate Tres Cordilleras Tradicional", Referencia = "CHTCT",
                    FechaIngreso = DateTime.Now,
                    TipoProducto = TipoProducto.Perecedero, Precio = 800
                },
                new Producto()
                {
                    Id = 0,
                    Nombre = "Chocolate de Mesa Corona Tradicional", Referencia = "CHMCT", FechaIngreso = DateTime.Now,
                    TipoProducto = TipoProducto.Perecedero, Precio = 200
                },
                new Producto()
                {
                    Id = 0,
                    Nombre = "Leche Deslactosada Parmala", Referencia = "LD", FechaIngreso = DateTime.Now,
                    TipoProducto = TipoProducto.Perecedero, Precio = 300
                },
                new Producto()
                {
                    Id = 0,
                    Nombre = "Vino Cerro Verde Chardonnay", Referencia = "VCVC", FechaIngreso = DateTime.Now,
                    TipoProducto = TipoProducto.Perecedero, Precio = 500
                },
                new Producto()
                {
                    Id = 0,
                    Nombre = "Tapabocas Anti-fluidos", Referencia = "TAF", FechaIngreso = DateTime.Now,
                    TipoProducto = TipoProducto.NoPerecedero, Precio = 800
                }
            );
            modelBuilder.Entity<ExistenciaProducto>().HasData(
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHTCT-001", ProductoId = 5
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHMCT-001", ProductoId = 1
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHMCT-002", ProductoId = 1
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHMCT-003", ProductoId = 1
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHMCT-004", ProductoId = 1
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "CHMCT-005", ProductoId = 1
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "LD-001", ProductoId = 2
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "LD-002", ProductoId = 2
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = true,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "LD-003", ProductoId = 2
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "VCVC-001", ProductoId = 3
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "VCVC-002", ProductoId = 3
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "VCVC-003", ProductoId = 3
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "VCVC-004", ProductoId = 3
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "VCVC-005", ProductoId = 3
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "TAF-001", ProductoId = 4
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "TAF-002", ProductoId = 4
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "TAF-003", ProductoId = 4
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "TAF-004", ProductoId = 4
                },
                new ExistenciaProducto()
                {
                    Id = 0,
                    Vendido = false,
                    EstadoProducto = EstadoProducto.Nuevo, Serie = "TAF-005", ProductoId = 4
                }
            );

            modelBuilder.Entity<Venta>().HasData(
                new Venta()
                {
                    Id = 0, ClienteId = 1, FehcaVenta = Convert.ToDateTime("2000-02-01 00:00:00.000000")
                },
                new Venta()
                {
                    Id = 0, ClienteId = 2, FehcaVenta = Convert.ToDateTime("2000-05-25 00:00:00.000000")
                },
                new Venta()
                {
                    Id = 0, ClienteId = 3, FehcaVenta = Convert.ToDateTime("2000-07-25 00:00:00.000000")
                },
                new Venta()
                {
                    Id = 0, ClienteId = 4, FehcaVenta = Convert.ToDateTime("2000-03-20 00:00:00.000000")
                }
            );

            modelBuilder.Entity<DetalleVentaProducto>().HasData(
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 1, VentaId = 4, ExistenciaProductoId = 1
                },
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 1, VentaId = 4, ExistenciaProductoId = 2
                },
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 2, VentaId = 1, ExistenciaProductoId = 6
                },
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 2, VentaId = 1, ExistenciaProductoId = 7
                },
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 2, VentaId = 2, ExistenciaProductoId = 8
                },
                new DetalleVentaProducto()
                {
                    Id = 0,
                    ProductoId = 3, VentaId = 3, ExistenciaProductoId = 9
                }
            );*/
        }
    }
}