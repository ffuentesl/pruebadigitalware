using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        bool Actualizar(TEntity entity);
        bool Crear(TEntity entity);
        void Eliminar(TEntity entity);
        public TEntity Obtener(int id, bool includeRelatedEntities = true);
        public IList<TEntity> ObtenerLista();
        Task<TEntity> CrearAsync(TEntity entity);
        Task<TEntity> ActualizarAsync(TEntity entity);
    }

    
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected DigitalWareContext Context { get; private set; }

        public BaseRepository(DigitalWareContext context)
        {
            Context = context;
        }

        public abstract TEntity Obtener(int id, bool includeRelatedEntities = true);
        
        public abstract IList<TEntity> ObtenerLista();

        public bool Crear(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            return (Context.SaveChanges()) > 0;
        }



        public bool Actualizar(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return (Context.SaveChanges()) > 0;
        }
        
        public Task<TEntity>  CrearAsync(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
            return Task.FromResult(entity); 
        }
        
        public Task<TEntity>  ActualizarAsync(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
            return Task.FromResult(entity);
        }
        
        public void Eliminar(TEntity entity)
        {
            Context.Remove(entity);
            Context.SaveChanges();
        }
 
 
    }

}