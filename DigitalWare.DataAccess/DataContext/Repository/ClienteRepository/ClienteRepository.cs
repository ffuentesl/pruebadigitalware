using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalWare.DataAccess.Entity.Usuario;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository.ClienteRepository
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {
        private readonly DigitalWareContext _context;

        public ClienteRepository(DigitalWareContext context) : base(context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Se utiliza para obtener el cliente por id
        /// </summary>
        /// <param name="id"> ClienteId</param>
        /// <param name="includeRelatedEntities"> Si la entidad tiene una relacion con otra entida </param>
        /// <returns></returns>
        public override Cliente Obtener(int id, bool includeRelatedEntities = true)
        {
            var cliente = _context.Clientes.AsQueryable();
            
            return cliente.Where(e => e.Id == id).FirstOrDefault();

        }
        
        /// <summary>
        /// Obtener listado de Cliente
        /// </summary>
        /// <returns></returns>
        public override IList<Cliente> ObtenerLista()
        {
            return _context.Clientes.OrderBy(c=>c.Id).ToList();
        }

        public async Task<Cliente> ObtenerPor(int identificacion)
        {

            return await _context.Clientes.FirstOrDefaultAsync(i => i.Identificacion == identificacion);
        }
        
        public async Task<bool> ExistePor(int id)
        {
            return await _context.Clientes.AnyAsync(f => f.Id == id);
        } 
        
        
    }
}