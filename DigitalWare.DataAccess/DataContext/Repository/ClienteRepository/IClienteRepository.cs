using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalWare.DataAccess.Entity.Usuario;

namespace DigitalWare.DataAccess.DataContext.Repository.ClienteRepository
{
    public interface IClienteRepository:IBaseRepository<Cliente>
    {
        /// <summary>
        /// Se utiliza para obtener el cliente por id
        /// </summary>
        /// <param name="id"> ClienteId</param>
        /// <param name="includeRelatedEntities"> Si la entidad tiene una relacion con otra entida </param>
        /// <returns></returns>
        Cliente Obtener(int id, bool includeRelatedEntities = true);

        /// <summary>
        /// Obtener listado de Cliente
        /// </summary>
        /// <returns></returns>
        IList<Cliente> ObtenerLista();

        Task<bool> ExistePor(int id);
        Task<Cliente> ObtenerPor(int identificacion);
    }
}