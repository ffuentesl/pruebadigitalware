using System.Collections.Generic;
using System.Linq;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository
{
    public interface IDetalleVentaProductoRepository :IBaseRepository<DetalleVentaProducto>
    {
        DetalleVentaProducto ObtenerDetalleVenta(int id,int ventaId, bool includeRelatedEntities = true);
        IList<DetalleVentaProducto> ObtenerLista();
        bool ExistePor(int id);
    }

    public class DetalleVentaProductoRepository : BaseRepository<DetalleVentaProducto>, IDetalleVentaProductoRepository
    {
        private readonly DigitalWareContext _context;

        public DetalleVentaProductoRepository(DigitalWareContext context) : base(context)
        {
            _context = context;
        }

        public  DetalleVentaProducto ObtenerDetalleVenta (int id, int ventaId,bool includeRelatedEntities = true)
        {
            var detalleVentaProducto = _context.DetalleVentaProductos.AsQueryable();

            if (includeRelatedEntities)
            {
                detalleVentaProducto = detalleVentaProducto.Include(i => i.Producto)
                    .Include(p => p.ExistenciaProducto)
                    .Include(v => v.Venta);
            }

            return detalleVentaProducto.Where(i => i.Id == id && i.VentaId == ventaId).FirstOrDefault();

        }

        public override DetalleVentaProducto Obtener(int id, bool includeRelatedEntities = true)
        {
            throw new System.NotImplementedException();
        }

        public override IList<DetalleVentaProducto> ObtenerLista()
        {
            return _context.DetalleVentaProductos.OrderBy(c=>c.Id).ToList();
        }
        public bool ExistePor(int id)
        {
            return _context.DetalleVentaProductos.Any(f => f.Id == id);
        }
    }
}