using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository
{
    public interface IExistenciaProductoRepository : IBaseRepository<ExistenciaProducto>
    {
        new ExistenciaProducto Obtener(int id, bool includeRelatedEntities = true);
        new IList<ExistenciaProducto> ObtenerLista();
        bool ExistePor(int id);
        Task<List<ExistenciaProductoDto>> ObtenerListaAsyncPor(int productoId, bool includeRelatedEntities = true);

        Task<ExistenciaProducto> ObtenerExistenciaAsyncPor(int productoId, string id,
            bool includeRelatedEntities = true);
        bool ExistePor(string serie);
    }

    public class ExistenciaProductoRepository:BaseRepository<ExistenciaProducto>, IExistenciaProductoRepository
    {
        private readonly DigitalWareContext _context;

        public ExistenciaProductoRepository(DigitalWareContext context) : base(context)
        {
            _context = context;
        }

        public override ExistenciaProducto Obtener(int id, bool includeRelatedEntities = true)
        {
            var existenciaProducto = _context.ExistenciaProductos.AsQueryable();

            if (includeRelatedEntities)
            {
                existenciaProducto = existenciaProducto.Include(e => e.Producto);
            }

           return existenciaProducto.Where(i => i.Id == id).FirstOrDefault();
        }

        public async Task<List<ExistenciaProductoDto>> ObtenerListaAsyncPor(int productoId, bool includeRelatedEntities = true)
        {
            var existenciaProducto = _context.ExistenciaProductos.AsQueryable();

            if (includeRelatedEntities)
            {
                existenciaProducto = existenciaProducto.Include(e => e.Producto);
            }

            return Mapper.Map<List<ExistenciaProductoDto>>(await existenciaProducto.Where(i => i.ProductoId == productoId).ToListAsync());

        }
        public async Task<ExistenciaProducto> ObtenerExistenciaAsyncPor(int productoId,string id, bool includeRelatedEntities = true)
        {
            var existenciaProducto = _context.ExistenciaProductos.AsQueryable();

            if (includeRelatedEntities)
            {
                existenciaProducto = existenciaProducto.Include(e => e.Producto);
            }

            return await existenciaProducto
                .Where(i => i.ProductoId == productoId && i.Serie == id)
                .FirstOrDefaultAsync();

        }


        public override IList<ExistenciaProducto> ObtenerLista()
        {
            return _context.ExistenciaProductos.OrderBy(c=>c.Id).ToList();
        }

        public bool ExistePor(int id)
        {
            return _context.ExistenciaProductos.Any(f => f.Id == id);
        }
        public bool ExistePor(string serie)
        {
            return _context.ExistenciaProductos.Any(f=>f.Serie.ToUpper() == serie.ToUpper());

        }
    }
}