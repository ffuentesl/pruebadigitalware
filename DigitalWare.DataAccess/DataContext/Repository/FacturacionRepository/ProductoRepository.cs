using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository
{
    public interface IProductoRepository: IBaseRepository<Producto>
    {
        Producto Obtener(int id, bool includeRelatedEntities = true);
        IList<Producto> ObtenerLista();
        IList<ProductoListarDto> ObtenerListaConTotalCantidades();
        bool ExistePor(int id);
        List<Producto> ObtenerListaProductos();
        List<ResumenProductos> ObtenerListaproductosMinimos(int cantidaMinima);
        Task<ProductoDto> ObtenerPor(string serie);
    }

    public class ProductoRepository:BaseRepository<Producto>, IProductoRepository
    {
        private readonly DigitalWareContext _context;

        public ProductoRepository(DigitalWareContext context) : base(context)
        {
            _context = context;
        }

        public override Producto Obtener(int id, bool includeRelatedEntities = true)
        {
            return _context.Productos.Find(id);
        }
        
        public async Task<ProductoDto> ObtenerPor(string serie)
        {
            var productObtenido = await _context.Productos
                .Include(l => l.ListaExistenciaDetalleProductos)
                .Where(p => p.ListaExistenciaDetalleProductos.Any(s => s.Serie == serie))
                .Select(r=> new ProductoDto
                {
                    Nombre = r.Nombre,
                    Referencia = r.Referencia,
                    FechaIngreso = r.FechaIngreso,
                    TipoProducto = r.TipoProducto,
                    ListaExistenciaDetalleProductos = Mapper.Map<List<ExistenciaProductoListarDto>>(r.ListaExistenciaDetalleProductos)
                })
                .FirstOrDefaultAsync();
          
            return productObtenido;
        }

        public override IList<Producto> ObtenerLista()
        {
            return _context.Productos
                .Include(l=>l.ListaExistenciaDetalleProductos)   
                /*
                .Where(r=>r.ListaExistenciaDetalleProductos.Any(l=>l.Vendido == false))
                */
                .Where(r=> r.ListaExistenciaDetalleProductos.Count > 0)
                .OrderBy(c=>c.Id).ToList();
            
        }
        
        public  IList<ProductoListarDto> ObtenerListaConTotalCantidades()
        {
            return _context.Productos
                .Include(l=>l.ListaExistenciaDetalleProductos)   
                .Select(r=> new ProductoListarDto
                {
                    Id = r.Id,
                    Nombre = r.Nombre,
                    Referencia = r.Referencia,
                    FechaIngreso = r.FechaIngreso,
                    TipoProducto = r.TipoProducto,
                    ListaExistenciaDetalleProductos = Mapper.Map<List<ExistenciaProductoListarDto>>(r.ListaExistenciaDetalleProductos),
                    NumeroProductos = r.ListaExistenciaDetalleProductos.Count
                    
                })
                .Where(r=> r.ListaExistenciaDetalleProductos.Count > 0)
                .OrderBy(c=>c.Id).ToList();
            
        }
        
        public List<Producto>  ObtenerListaProductos()
        {
            var propducto = _context.Productos.Include(p=>p.ListaExistenciaDetalleProductos)
                .ToList();
            return propducto;
        } 
        /// <summary>
        ///  obtine lista de productos menor o igual a la cantidad envia 
        /// </summary>
        /// <param name="cantidaMinima"> valor minimo de cantidades del producto a buscar </param>
        /// <returns>lista con los productos que cumple la condicion de menor o igual</returns>
        public List<ResumenProductos>  ObtenerListaproductosMinimos(int cantidaMinima)
        {
            var propducto = _context.Productos
                .Include(p=>p
                    .ListaExistenciaDetalleProductos)
                .Select( r=> new ResumenProductos
                {
                    Nombre = r.Nombre,
                    Referencia =  r.Referencia,
                    TipoProducto = r.TipoProducto,
                    NumeroCantidades = r.ListaExistenciaDetalleProductos.Count()
                    
                }).Where(r=>r.NumeroCantidades <= cantidaMinima)
                .ToList();
            
            return propducto;
        }
        
        public bool ExistePor(int id)
        {
            return _context.Productos.Any(f=>f.Id == id);

        }

        
    }
}