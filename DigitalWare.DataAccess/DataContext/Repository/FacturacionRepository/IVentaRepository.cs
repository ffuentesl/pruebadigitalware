using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalWare.DataAccess.Entity.Facturacion;

namespace DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository
{
    public interface IVentaRepository:IBaseRepository<Venta>
    {
        new Venta Obtener(int id, bool includeRelatedEntities = true);
        Task<Venta> ObtenerVentaPor(int IdentificacionCliente);
        new IList<Venta> ObtenerLista();

        List<ResumenClientesPorEdadFechaCompra> ObtenerListaClientesPorEdad(DateTime fechaInicio, DateTime fechaFinal,
            int edad);
        
        List<ResumenProducto> ValorTotalProductosPro(DateTime fechaCompra);
    }
}