using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.EntityFrameworkCore;

namespace DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository
{
    public class VentaRepository : BaseRepository<Venta>, IVentaRepository
    {
        private readonly DigitalWareContext _context;

        public VentaRepository(DigitalWareContext context) : base(context)
        {
            _context = context;
        }

        public override Venta Obtener(int id, bool includeRelatedEntities = true)
        {
            var venta = _context.Ventas.AsQueryable();

            if (includeRelatedEntities)
            {
                venta = venta.Include(i => i.Cliente);
                venta = venta.Include(l => l.ListaDetalleVentaProducto); 
            }
  
            return venta.Where(i => i.Id == id).FirstOrDefault();
        }
        /// <summary>
        /// obtiene lista de producto por fecha de inicio, fin y edad
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFinal"></param>
        /// <param name="edad"></param>
        /// <returns>retorna lista resumen clientes por edad y fechas de compras</returns>
        public List<ResumenClientesPorEdadFechaCompra> ObtenerListaClientesPorEdad(DateTime fechaInicio, DateTime fechaFinal, int edad)
        {
            var lista = _context.Ventas.Include(c => c.Cliente)
                .Select(r => new ResumenClientesPorEdadFechaCompra
                {
                    ClienteNombre =$"{r.Cliente.Nombre} {r.Cliente.Apellido}",
                    ClienteId = r.ClienteId,
                    FechaVenta =  r.FehcaVenta,
                    ClienteEdad = DateTime.Now.Year - r.Cliente.FechaNacimiento.Year
                })
                .Where(v=>v.FechaVenta >= fechaInicio && v.FechaVenta <= fechaFinal  && v.ClienteEdad < edad)
                .ToList();
            
            return lista;
        }

        /// <summary>
        ///  obtine lista de productos con el valor total vendidos en la fecha 
        /// </summary>
        /// <param name="fechaCompra"> DateTime </param>
        /// <returns>Lista de ValortTotalProducto</returns>
        public List<ResumenProducto> ValorTotalProductosPro( DateTime fechaCompra)
        {
            
            var lista = _context.DetalleVentaProductos
                .Include(p=>p.Producto)
                .Include(v=>v.Venta)
                .Where(f=>f.Venta.FehcaVenta <= fechaCompra)
                .GroupBy(g => new {g.ProductoId, g.Producto})
                .Select(r => new ResumenProducto
                {
                    Producto = r.Key.Producto,
                    NumeroProductos = r.Count(),
                    
                })
                .ToList();

            return lista;
        }
        
        public async Task<Venta> ObtenerVentaPor(int IdentificacionCliente)
        {
            return await _context.Ventas.Include(i => i.Cliente)
                .Where(i => i.Cliente.Identificacion == IdentificacionCliente).FirstOrDefaultAsync();
            
        } 
        
        
        public override IList<Venta> ObtenerLista()
        {
            return _context.Ventas
                .Include(c=>c.Cliente)
                .Include(d=>d.ListaDetalleVentaProducto)
                         .ThenInclude(l=>l.Producto)
                .OrderBy(c=>c.Id).ToList();
        }
    }
}