namespace DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion
{
    public enum EstadoProducto
    {
        Nuevo,
        Reservado,
        Descompuesto,
        SinGarantia,
    }
}