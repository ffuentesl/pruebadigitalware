namespace DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion
{
    public enum TipoProducto
    {
        Bruto,
        Elaborados,
        Perecedero,
        NoPerecedero,
        Fragiles,
        Peligrosas,
        Generico
    }
}