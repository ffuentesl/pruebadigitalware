using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;

namespace DigitalWare.DataAccess.Entity.Facturacion
{
    [Table("ExistenciaProducto",Schema = "Facturacion")]
    public class ExistenciaProducto
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public bool Vendido { get; set; }
        [Required]
        public EstadoProducto EstadoProducto { get; set; }
        
        [Required]
        public string Serie { get; set; } 
        public Producto Producto { get; set; }
        [Required]
        public int ProductoId { get; set; }
    }
}