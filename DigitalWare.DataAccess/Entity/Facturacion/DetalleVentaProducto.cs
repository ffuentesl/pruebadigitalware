using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalWare.DataAccess.Entity.Facturacion
{
    [Table("DetalleVentaProducto",Schema = "Facturacion")]
    public class DetalleVentaProducto
    {
        [Key]
        public int Id { get; set; }

        public Venta Venta { get; set; }
        [Required]
        public int VentaId { get; set; }

        public Producto Producto { get; set; }
        [Required]
        public int ProductoId { get; set; }

        public ExistenciaProducto ExistenciaProducto { get; set; }
        public int ExistenciaProductoId { get; set; }
    }
}