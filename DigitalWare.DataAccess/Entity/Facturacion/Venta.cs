using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DigitalWare.DataAccess.Entity.Usuario;

namespace DigitalWare.DataAccess.Entity.Facturacion
{
    [Table("Venta",Schema = "Facturacion")]
    public class Venta
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime FehcaVenta { get; set; }
        
        public Cliente Cliente { get; set; }
        [Required]
        public int ClienteId { get; set; }

        public List<DetalleVentaProducto> ListaDetalleVentaProducto { get; set; }
    }
}