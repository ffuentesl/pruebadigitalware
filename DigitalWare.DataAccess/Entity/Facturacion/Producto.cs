using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;

namespace DigitalWare.DataAccess.Entity.Facturacion
{
    [Table("Producto",Schema = "Facturacion")]
    public class Producto
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Referencia { get; set; }

        public TipoProducto TipoProducto { get; set; } = TipoProducto.Generico;
        public DateTime FechaIngreso { get; set; } = DateTime.Now;

        public int Precio { get; set; }

        public List<ExistenciaProducto> ListaExistenciaDetalleProductos { get; set; }
    }
}