using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalWare.DataAccess.Entity.Usuario
{
    [Table("Cliente",Schema = "ClienteDigitalWare")]
    public class Cliente
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(50)]
        public string Apellido { get; set; }
        [Required]
        [MaxLength(20)] 
        public int Identificacion { get; set; }
        [Required]
        [MaxLength(20)]
        public string TelefonoCelularFijo { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }


        
    }
}