using System.ComponentModel.DataAnnotations;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;


namespace DigitalWare.DataAccess.Dto.ProductoDtos
{
    public class ExistenciaProductoDto : IMapFrom<ExistenciaProducto>
    {
        
 
        [Required]
        public bool Vendido { get; set; }
        [Required]
        public EstadoProducto EstadoProducto { get; set; }
        [Required]
        public string Serie { get; set; } 
        
        /*public ProductoDto Producto { get; set; }*/
       
       // public int ProductoId { get; set; }
        
    }
    public class ExistenciaProductoEditarDto:ExistenciaProductoDto
    {
        [Required]
        public int Id { get; set; }

    }   
    
    public class ExistenciaProductoListarDto:ExistenciaProductoDto
    {
        public int Id { get; set; }

    }
    public class ExistenciaProductoResumenDto
    {
       
        public ProductoDto Producto { get; set; }

    }
}