namespace DigitalWare.DataAccess.Dto.ProductoDtos
{
    public class ProductoListarDto:ProductoDto 
    {
        public int Id { get; set; }
        public int NumeroProductos { get; set; }
        
    }
}