using System;
using System.ComponentModel.DataAnnotations;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;

namespace DigitalWare.DataAccess.Dto.ProductoDtos
{
    public class ProductoEditarDto :IMapFrom<Producto>
    {
        [Required]
        public string Nombre { get; set; }
  
        [Required]
        public string Referencia { get; set; }
        [Required]
        public TipoProducto TipoProducto { get; set; } 
        [Required]
        public DateTime FechaIngreso { get; set; }
     
    }
}