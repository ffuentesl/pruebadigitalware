using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;

namespace DigitalWare.DataAccess.Dto.ProductoDtos
{
    public class ProductoDto : IMapFrom<Producto>
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
  
        [Required]
        public string Referencia { get; set; }
        [Required]
        public TipoProducto TipoProducto { get; set; } = TipoProducto.Generico;
        [Required]
        public DateTime FechaIngreso { get; set; } = DateTime.Now;

        public List<ExistenciaProductoListarDto> ListaExistenciaDetalleProductos { get; set; }
        

    }
    public class ProductoCrearDto : IMapFrom<Producto>
    {
        public int Id { get; set; }

        [Required]
        public string Nombre { get; set; }
  
        [Required]
        public string Referencia { get; set; }
        [Required]
        public TipoProducto TipoProducto { get; set; } = TipoProducto.Generico;
        [Required]
        public DateTime FechaIngreso { get; set; } = DateTime.Now;
        
    }
    public class ResumenProductos
    {
       
        public string Nombre { get; set; }
        public string Referencia { get; set; }
        public TipoProducto TipoProducto { get; set; }
        public int NumeroCantidades { get; set; }
 
    }
}