using System;
using System.ComponentModel.DataAnnotations;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Entity.Usuario;

namespace DigitalWare.DataAccess.Dto.Usuario
{
    public class ClienteDto : IMapFrom<Cliente>
    {
        [Required] public string Nombre { get; set; }
        [Required] public string Apellido { get; set; }
        [Required] public string TelefonoCelularFijo { get; set; }
        [Required] public int Identificacion { get; set; }
        [Required] public string FechaNacimiento { get; set; }
    }
}