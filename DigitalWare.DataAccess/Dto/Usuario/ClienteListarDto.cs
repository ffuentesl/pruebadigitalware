namespace DigitalWare.DataAccess.Dto.Usuario
{
    public class ClienteListarDto : ClienteDto
    {
        public int Id { get; set; }
    }
}