using System.ComponentModel.DataAnnotations;

namespace DigitalWare.DataAccess.Dto.Usuario
{
    public class ClienteEditarDto : ClienteDto
    {
        [Required] public int Id { get; set; }
    }
}