using System.ComponentModel.DataAnnotations;

namespace DigitalWare.DataAccess.Dto.VentaDtos
{
    public class DetalleVentaProductoEditarDto : DetalleVentaProductoDto
    {
        [Required] public int Id { get; set; }
    }
}