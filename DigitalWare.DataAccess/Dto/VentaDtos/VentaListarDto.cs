namespace DigitalWare.DataAccess.Dto.VentaDtos
{
    public class VentaListarDto : VentaDto
    {
        public int Id { get; set; }
    }
}