using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Dto.Usuario;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Usuario;

namespace DigitalWare.DataAccess.Dto.VentaDtos
{
    public class VentaDto: IMapFrom<Venta>
    {
        
        [Required] public string FehcaVenta { get; set; }

        public ClienteDto Cliente { get; set; }
        [Required] public int ClienteId { get; set; }

        public List<DetalleVentaProductoListarDto> ListaDetalleVentaProducto { get; set; }
    }
    
    public class VentaCrearDto: IMapFrom<Venta>
    {
        
        [Required] public string FehcaVenta { get; set; } 

       
        [Required] public int ClienteId { get; set; }

    }

}



public class ResumenClientesPorEdadFechaCompra
{
    public int ClienteId { get; set; }
    public string ClienteNombre { get; set; }
    public int ClienteEdad{ get; set; }
    public DateTime FechaVenta { get; set; }
    
}

public class ResumenProducto
{
  
   
    
    
    public int NumeroProductos { get; set; }
    public Producto Producto { get; set; }

}
public class ValorTotalProducto
{
    public string ProductoNombre { get; set; }
    public int ProductoValor { get; set; }
    public int ValorTotal { get; set; }
    
    

}