using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;

namespace DigitalWare.DataAccess.Dto.VentaDtos
{
    public class DetalleVentaProductoListarDto : DetalleVentaProductoDto
    {
        public int Id { get; set; }
        
        public ProductoDto Producto { get; set; }


    }
}