using System.ComponentModel.DataAnnotations;
using AutoMapper;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;

namespace DigitalWare.DataAccess.Dto.VentaDtos
{
    public class DetalleVentaProductoDto:IMapFrom<DetalleVentaProducto>
    {
        [Required] public int ProductoId { get; set; }
        
        public int ExistenciaProductoId { get; set; }

    }
}