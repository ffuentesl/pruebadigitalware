using System;
using AutoMapper;

namespace DigitalWare.DataAccess.Dto.PaginarData
{
    public interface ICustomMapFrom
    {
        void CreateCustomMapping(IMapperConfigurationExpression cfg);
    }
    
}

