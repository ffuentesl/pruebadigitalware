using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace DigitalWare.DataAccess.Dto.PaginarData
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings(IMapperConfigurationExpression cfg)
        {
            Type[] exportedTypes = Assembly.GetExecutingAssembly().GetExportedTypes();
            var datas = ((IEnumerable<Type>) exportedTypes).SelectMany(i => (IEnumerable<Type>) i.GetInterfaces(),
                (t, i) => new
                {
                    i = i,
                    t = t
                }).Where(f =>
            {
                if (f.i.IsGenericType && f.i.GetGenericTypeDefinition() == typeof(IMapFrom<>) && !f.t.IsAbstract)
                    return !f.t.IsInterface;
                return false;
            }).Select(r => new
            {
                Source = ((IEnumerable<Type>) r.i.GetGenericArguments()).First<Type>(),
                Destination = r.t
            });
            List<ICustomMapFrom> list = ((IEnumerable<Type>) exportedTypes).SelectMany(
                (Func<Type, IEnumerable<Type>>) (i => (IEnumerable<Type>) i.GetInterfaces()), (t, i) => new
                {
                    i = i,
                    t = t
                }).Where(f =>
            {
                if (typeof(ICustomMapFrom).IsAssignableFrom(f.t) && !f.t.IsAbstract)
                    return !f.t.IsInterface;
                return false;
            }).Select(r => (ICustomMapFrom) Activator.CreateInstance(r.t)).ToList<ICustomMapFrom>();
            foreach (var data in datas)
            {
                cfg.CreateMap(data.Source, data.Destination);
                cfg.CreateMap(data.Destination, data.Source);
            }

            foreach (ICustomMapFrom customMapFrom in list)
                customMapFrom.CreateCustomMapping(cfg);
        }
    }
}