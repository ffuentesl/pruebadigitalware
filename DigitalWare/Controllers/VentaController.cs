/*using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.VentaDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]/[action]")]
    [ApiConventionType(typeof(PersonalizarResuestaHttp))]
    public class VentaController : ControllerBase
    {
        private readonly ILogger<VentaController> _logger;
        private readonly IVentaRepository _ventaRepository;
        private readonly IDetalleVentaProductoRepository _detalleVentaProductoRepository;

        public VentaController(ILogger<VentaController> logger, 
            IVentaRepository ventaRepository, IDetalleVentaProductoRepository detalleVentaProductoRepository)
        {
            _logger = logger;
            _ventaRepository = ventaRepository;
            _detalleVentaProductoRepository = detalleVentaProductoRepository;
        }

        [HttpGet]
        public IActionResult ObtenerListadoVentas()
        {
            var listado = _ventaRepository.ObtenerLista();
            var mapperListado = Mapper.Map<List<VentaListarDto>>(listado);
            return Ok(mapperListado);
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] VentaDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                
                var listaDetalleVentaProducto = dto.ListaDetalleVentaProducto;
                
                var crearVenta = Mapper.Map<Venta>(dto);
                /*crearVenta.FehcaVenta = Convert.ToDateTime(dto.FehcaVenta);#1#

                var creadoVenta = await _ventaRepository.CrearAsync(crearVenta);

                _ventaRepository.Obtener(creadoVenta.Id); 
                var respuestaDetalleVenta =
                    Mapper.Map<List<DetalleVentaProductoDto>>(creadoVenta.ListaDetalleVentaProducto);
                creadoVenta.ListaDetalleVentaProducto = null;
                var respuesta = Mapper.Map<VentaListarDto>(creadoVenta);
                
               
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut]
        public async Task<IActionResult>  Editar([FromBody] VentaEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                /*if (!_ventaRepository.ExistePor(dto.Id))
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");#1#

                var editarVenta = _ventaRepository.Obtener(dto.Id);

                if (editarVenta == null)
                {
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");
                }
                
                editarVenta.FehcaVenta = Convert.ToDateTime(dto.FehcaVenta);

                var editar = await _ventaRepository.ActualizarAsync(editarVenta);

                return Ok(Mapper.Map<VentaEditarDto>(editar));
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                var eliminarVenta = _ventaRepository.Obtener(id);
                if (eliminarVenta == null)
                    return NotFound($"No hay resultados disponibles para el id {id}");

                //var producto = _ventaRepository.Obtener(id);

                _ventaRepository.Eliminar(eliminarVenta);

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }

        }
    }
}*/