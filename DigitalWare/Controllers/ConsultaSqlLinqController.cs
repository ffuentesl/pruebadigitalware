using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using Microsoft.AspNetCore.Mvc;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [ApiController]
    [Route("[controller]")]
    public class ConsultaSqlLinqController : ControllerBase
    {
        
        private readonly IProductoRepository _productoRepository;
        private readonly IExistenciaProductoRepository _existenciaProductoRepository;
        private readonly IVentaRepository _ventaRepository;

        public ConsultaSqlLinqController(
            IProductoRepository productoRepository, 
            IExistenciaProductoRepository existenciaProductoRepository,
            IVentaRepository ventaRepository)
        {
            
            _productoRepository = productoRepository;
            _existenciaProductoRepository = existenciaProductoRepository;
            _ventaRepository = ventaRepository;
        }

        [HttpGet]
        public IActionResult  Get()
        {
            //Obtener la lista de precios de todos los productos
            var listaProducto = Mapper.Map<List<ProductoListarDto>>(_productoRepository.ObtenerLista());
            

            //var maperListaProductos = Mapper.Map<List<ProductoListarDto>>(listaProducto);
            // cantidadMinaPorProducto=> Obtener la lista de productos cuya existencia en el inventario haya llegado al mínimo permitido (5 unidades)
            var cantidadMinaPorProducto = _productoRepository.ObtenerListaproductosMinimos(5);


            //Obtener una lista de clientes no mayores de 35 años que hayan realizado compras entre el
            //1 de febrero de 2000 y el 25 de mayo de 2000
            var listaPorEdadFechas = _ventaRepository.ObtenerListaClientesPorEdad(new DateTime(2000,02,25), new DateTime(2000,05,25),35 );
            
            // valorTotalProducto => Obtener el valor total vendido por cada producto en el año 2000
            var listaValorTotalProducto = _ventaRepository.ValorTotalProductosPro(new DateTime( 2000,4,1))
            .Select(v => new ValorTotalProducto
                {
                    ProductoNombre = v.Producto.Nombre,
                    ProductoValor = v.Producto.Precio,
                    ValorTotal = v.Producto.Precio * v.NumeroProductos,
                })
                .ToList();

            var respuesta = new
            {
                listaProducto,
                cantidadMinaPorProducto,
                listaPorEdadFechas,
                listaValorTotalProducto
            };
            
            return   Ok(respuesta);
        }
    }
}