/*using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.ClienteRepository;
using DigitalWare.DataAccess.Dto.Usuario;
using DigitalWare.DataAccess.Entity.Usuario;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;


namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]/[action]")]
    [ApiConventionType(typeof(PersonalizarResuestaHttp))]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly ILogger<ClienteController> _logger;
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(ILogger<ClienteController> logger,
            IClienteRepository clienteRepository, LinkGenerator linkGenerator)
        {
            _logger = logger;
            _clienteRepository = clienteRepository;
        }

        [HttpGet]
        public IActionResult ObtenerListaCliente()
        {
            var listaado = _clienteRepository.ObtenerLista();
            var mapperListado = Mapper.Map<List<ClienteListarDto>>(listaado);
            return Ok(mapperListado);
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] ClienteDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var clienteNuevo = Mapper.Map<Cliente>(dto);
                clienteNuevo.FechaNacimiento = Convert.ToDateTime(dto.FechaNacimiento);
                var creadoCliente = await _clienteRepository.CrearAsync(clienteNuevo);

                return Ok(creadoCliente);
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut]
        public async Task<IActionResult> Editar([FromBody] ClienteEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                if (!await _clienteRepository.ExistePor(dto.Id))
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");

                var editarProducto = _clienteRepository.Obtener(dto.Id);


                _clienteRepository.Actualizar(editarProducto);

                return Ok("Proceso termino exitosamente");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                if (!await _clienteRepository.ExistePor(id))
                    return NotFound($"No hay resultados disponibles para el id {id}");


                _clienteRepository.Eliminar(_clienteRepository.Obtener(id));

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
    }
}*/