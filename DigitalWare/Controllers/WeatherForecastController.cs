﻿using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IProductoRepository _productoRepository;
        private readonly IExistenciaProductoRepository _existenciaProductoRepository;
        private readonly IVentaRepository _ventaRepository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            IProductoRepository productoRepository, 
            IExistenciaProductoRepository existenciaProductoRepository,
            IVentaRepository ventaRepository)
        {
            _logger = logger;
            _productoRepository = productoRepository;
            _existenciaProductoRepository = existenciaProductoRepository;
            _ventaRepository = ventaRepository;
        }

        [HttpGet]
        public IActionResult  Get()
        {
            return   Ok("OK");
        }
    }
}