/*using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]/[action]")]
    [ApiConventionType(typeof(PersonalizarResuestaHttp))]
    public class ExistenciaProductoController :ControllerBase
    {
        private readonly ILogger<ExistenciaProductoController> _logger;
        private readonly IExistenciaProductoRepository _existenciaProductoRepository;

        public ExistenciaProductoController(ILogger<ExistenciaProductoController> logger,
            IExistenciaProductoRepository  existenciaProductoRepository)
        {
            _logger = logger;
            _existenciaProductoRepository = existenciaProductoRepository;
        }
       
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] ExistenciaProductoDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            { 
                var creadoProducto = await _existenciaProductoRepository.CrearAsync(Mapper.Map<ExistenciaProducto>(dto));

                return Ok(creadoProducto);
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        
        [HttpPut]
        public IActionResult Editar([FromBody] ExistenciaProductoEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                if (!_existenciaProductoRepository.ExistePor(dto.Id))
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");

                var editarProducto = _existenciaProductoRepository.Obtener(dto.Id);
                editarProducto.EstadoProducto = dto.EstadoProducto;
                editarProducto.Serie = dto.Serie;
                editarProducto.Vendido = dto.Vendido;

                
                _existenciaProductoRepository.Actualizar(editarProducto);
                
                return Ok("Proceso termino exitosamente");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        
        [HttpDelete("{id:int}")]
        public IActionResult Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                if (!_existenciaProductoRepository.ExistePor(id))
                    return NotFound($"No hay resultados disponibles para el id {id}");

                //var producto = _existenciaProductoRepository.Obtener(id);

                _existenciaProductoRepository.Eliminar(_existenciaProductoRepository.Obtener(id));

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        
        /// <summary>
        /// Emun  EstadoProducto retorna la lista   
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ObtenerEstadoProducto()
        {
            Dictionary<string, int> lista = new Dictionary<string, int>();
            foreach (var item in Enum.GetNames(typeof(EstadoProducto)))
            {
                lista.Add(item, (int) Enum.Parse(typeof(EstadoProducto), item));
            }

            return Ok(lista);
        }
        
        
        
    }
}*/