using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.VentaDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json", "application/xml")]
    [ApiController]
    public class VentaDWController : ControllerBase
    {
        private readonly ILogger<VentaDWController> _logger;
        private readonly IVentaRepository _ventaRepository;
        private readonly IDetalleVentaProductoRepository _detalleVentaProductoRepository;
        private readonly LinkGenerator _linkGenerator;

        public VentaDWController(ILogger<VentaDWController> logger,
            IVentaRepository ventaRepository,
            IDetalleVentaProductoRepository detalleVentaProductoRepository,
            LinkGenerator linkGenerator)
        {
            _logger = logger;
            _ventaRepository = ventaRepository;
            _detalleVentaProductoRepository = detalleVentaProductoRepository;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public IActionResult ObtenerListadoVentas()
        {
            var listado = _ventaRepository.ObtenerLista();
            var mapperListado = Mapper.Map<List<VentaListarDto>>(listado);
            return Ok(mapperListado);
        }
        
        [HttpGet("{id:int}")]
        public ActionResult<VentaDto> Get(int id, bool incluirDatos = false)
        {
            var venta = _ventaRepository.Obtener(id, incluirDatos);
            
            return Ok(Mapper.Map<VentaDto>(venta));
        }
        
        [HttpPost]
        public async Task<ActionResult<VentaListarDto>> Post(VentaCrearDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                
                var crearVenta = Mapper.Map<Venta>(dto);
                var creadoVenta = await _ventaRepository.CrearAsync(crearVenta);
                
                return Created($"/api/ventaDw/{creadoVenta.Id}", Mapper.Map<VentaListarDto>(creadoVenta));

            }
            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<VentaEditarDto>> Put(int id, VentaEditarDto dto)
        {
            try
            { 
                var venta = _ventaRepository.Obtener(id, false);

                var editarVenta = Mapper.Map(dto, venta);

                var editado = await _ventaRepository.ActualizarAsync(editarVenta);

                return Mapper.Map<VentaEditarDto>(editado);

            }
            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

    }
}