/*using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Facturacion.EnumFacturacion;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]/[action]")]
    [ApiConventionType(typeof(PersonalizarResuestaHttp))]
    public class ProductoController : ControllerBase
    {
        private readonly ILogger<ProductoController> _logger;
        private readonly IProductoRepository _productoRepository;

        public ProductoController(ILogger<ProductoController> logger,
            IProductoRepository productoRepository)
        {
            _logger = logger;
            _productoRepository = productoRepository;
        }

        [HttpGet]
        public IActionResult ObtenerListado()
        {
            var listado = _productoRepository.ObtenerLista();
            var mapperListado = Mapper.Map<List<ProductoListarDto>>(listado);
            return Ok(mapperListado);
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] ProductoDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                Producto producto;

                producto = Mapper.Map<Producto>(dto);
                
               var crearProducto = await _productoRepository.CrearAsync(producto);
               
                return Ok(Mapper.Map<ProductoDto>(crearProducto));
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut()]
        public IActionResult Editar([FromBody] ProductoEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                if (!_productoRepository.ExistePor(dto.Id))
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");

                var editarProducto = _productoRepository.Obtener(dto.Id);
                editarProducto.Nombre = dto.Nombre;
                editarProducto.Referencia = dto.Referencia;
                editarProducto.TipoProducto = dto.TipoProducto;
                
                _productoRepository.Actualizar(editarProducto);
                
                return Ok("Proceso termino exitosamente");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                if (!_productoRepository.ExistePor(id))
                    return NotFound($"No hay resultados disponibles para el id {id}");

                //var producto = _productoRepository.Obtener(id);

                _productoRepository.Eliminar(_productoRepository.Obtener(id));

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        
        
        [HttpGet]
        public IActionResult ObtenerTipoProducto()
        {
            Dictionary<string, int> lista = new Dictionary<string, int>();
            foreach (var item in Enum.GetNames(typeof(TipoProducto)))
            {
                lista.Add(item, (int) Enum.Parse(typeof(TipoProducto), item));
            }

            return Ok(lista);
        }
  

    }
}*/