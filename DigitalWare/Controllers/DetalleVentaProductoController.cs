/*using System;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.VentaDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]/[action]")]
    [ApiConventionType(typeof(PersonalizarResuestaHttp))]
    public class DetalleVentaProductoController :ControllerBase
    {
        private readonly ILogger<DetalleVentaProductoController> _logger;
        private readonly IDetalleVentaProductoRepository _detalleVentaProductoRepository;

        public DetalleVentaProductoController(ILogger<DetalleVentaProductoController> logger,
             IDetalleVentaProductoRepository detalleVentaProductoRepository)
        {
            _logger = logger;
            _detalleVentaProductoRepository = detalleVentaProductoRepository;
        }
        
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<IActionResult> Crear([FromBody] DetalleVentaProductoDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            { 
                var creadoProducto = await _detalleVentaProductoRepository.CrearAsync(Mapper.Map<DetalleVentaProducto>(dto));

                return Ok(creadoProducto);
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        
        /*
        [HttpPut]
        public async Task<IActionResult> Editar([FromBody] DetalleVentaProductoEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                if (!_detalleVentaProductoRepository.ExistePor(dto.Id))
                    return NotFound($"No hay resultados disponibles para el id {dto.Id}");

                var editarProducto = _detalleVentaProductoRepository.Obtener(dto.Id);


                
                _detalleVentaProductoRepository.Actualizar(editarProducto);
                
                return Ok("Proceso termino exitosamente");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
        #1#
        
        [HttpDelete("{id:int}")]
        public IActionResult Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                if (!_detalleVentaProductoRepository.ExistePor(id))
                    return NotFound($"No hay resultados disponibles para el id {id}");

                //var producto = _detalleVentaProductoRepository.Obtener(id);

                _detalleVentaProductoRepository.Eliminar(_detalleVentaProductoRepository.Obtener(id));

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
     
        
        
    }
}*/