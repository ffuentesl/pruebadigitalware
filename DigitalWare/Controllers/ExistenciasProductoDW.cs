using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Route("api/productoDW/{productoId}/existenciaProductoDw")]
    [Produces("application/json", "application/xml")]
    [ApiController]
    public class ExistenciasProductoDW : ControllerBase
    {
        private readonly ILogger<ExistenciasProductoDW> _logger;
        private readonly IExistenciaProductoRepository _existenciaProductoRepository;
        private readonly LinkGenerator _linkGenerator;
        private readonly IProductoRepository _productoRepository;

        public ExistenciasProductoDW(ILogger<ExistenciasProductoDW> logger,
            IExistenciaProductoRepository existenciaProductoRepository,
            LinkGenerator linkGenerator, IProductoRepository productoRepository)
        {
            _logger = logger;
            _existenciaProductoRepository = existenciaProductoRepository;
            _linkGenerator = linkGenerator;
            _productoRepository = productoRepository;
        }


        [HttpGet]
        public async Task<ActionResult<List<ExistenciaProductoDto>>> Get(int productoId)
        {
            var listaProductos = await _existenciaProductoRepository.ObtenerListaAsyncPor(productoId: productoId,
                includeRelatedEntities: false);

            return listaProductos;
        }

        [HttpGet("{serie}")]
        public async Task<ActionResult<ExistenciaProductoResumenDto>> Get(int productoId, string serie)
        {
            var existenciaProductoResumenDto = await _existenciaProductoRepository.ObtenerExistenciaAsyncPor(productoId: productoId,
                id: serie, includeRelatedEntities: true);

            return Mapper.Map<ExistenciaProductoResumenDto>(existenciaProductoResumenDto);
        }

        [HttpPost]
        public ActionResult<ExistenciaProductoListarDto> Post(int productoId, ExistenciaProductoDto dto)
        {
            try
            {
                if(_existenciaProductoRepository.ExistePor(dto.Serie))
                    return BadRequest($"Esta serie no esrta disponible serie: {dto.Serie}");

                if (!_productoRepository.ExistePor(id: productoId))
                    return BadRequest($"No hay resultados disponible para el Id: {productoId}");
               
                var crear = Mapper.Map<ExistenciaProducto>(dto);
                
                crear.ProductoId = productoId;

                if (_existenciaProductoRepository.Crear(crear))
                {
                    var url = _linkGenerator.GetPathByAction(HttpContext,
                        "Get", values: new {productoId, serie = crear.Serie});

                    return Created(url, Mapper.Map<ExistenciaProductoListarDto>(crear));
                }
                else
                {
                    return BadRequest("Error al guardar los datos ");
                }
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error de base datos");
            }
        }

        [HttpPut("{serie}")]
        public async Task<ActionResult<ExistenciaProductoListarDto>> Put(int productoId, string serie, ExistenciaProductoDto dto)
        {
            try
            {
                var existenciaProducto = await _existenciaProductoRepository.ObtenerExistenciaAsyncPor(productoId: productoId,
                    id: serie, includeRelatedEntities: false);
               
                if (existenciaProducto == null) return BadRequest($"Dato no disponible id: {productoId}");

                if(dto.Serie != existenciaProducto.Serie)
                    if (_existenciaProductoRepository.ExistePor(dto.Serie))
                        return BadRequest($"Esta serie: {dto.Serie} no esta disponible ");
                
                
                var mapperexistenciaProducto = Mapper.Map(dto, existenciaProducto);

                if (_existenciaProductoRepository.Actualizar(mapperexistenciaProducto))
                {
                    return Mapper.Map<ExistenciaProductoListarDto>(mapperexistenciaProducto);
                }
                else
                {
                    return BadRequest("Error en la actualizacon");

                }
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed ExistenciaProductoListarDto");

            }
        }
    }
}