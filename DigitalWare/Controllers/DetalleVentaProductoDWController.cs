using System;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.VentaDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Route("api/ventaDW/{ventaId}/DetalleVentaProductoDW")]
    [Produces("application/json", "application/xml")]
    [ApiController]
    public class DetalleVentaProductoDWController : ControllerBase
    {
        private readonly ILogger<DetalleVentaProductoDWController> _logger;
        private readonly IDetalleVentaProductoRepository _detalleVentaProductoRepository;
        private readonly LinkGenerator _linkGenerator;

        public DetalleVentaProductoDWController(ILogger<DetalleVentaProductoDWController> logger,
            IDetalleVentaProductoRepository detalleVentaProductoRepository, LinkGenerator linkGenerator)
        {
            _logger = logger;
            _detalleVentaProductoRepository = detalleVentaProductoRepository;
            _linkGenerator = linkGenerator;
        }

        [HttpGet("{id:int}")]
        public ActionResult<DetalleVentaProductoListarDto> Get(int ventaId, int id, bool incluirDatos = false)
        {
            var lista = _detalleVentaProductoRepository.ObtenerDetalleVenta(id, ventaId, incluirDatos);

            return Mapper.Map<DetalleVentaProductoListarDto>(lista);
        }

        [HttpPost]
        public async Task<ActionResult<DetalleVentaProductoDto>> Post(int ventaId, DetalleVentaProductoDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var crearDetalleVenta = Mapper.Map<DetalleVentaProducto>(dto);
                crearDetalleVenta.VentaId = ventaId;


                // return Created($"/api/ventaDw/{creadoVenta.Id}", Mapper.Map<VentaCrearDto>(creadoVenta));

                if (_detalleVentaProductoRepository.Crear(crearDetalleVenta))
                {
                    var url = _linkGenerator.GetPathByAction(HttpContext,
                        "Get", values: new {ventaId, id = crearDetalleVenta.Id});

                    return Created(url, Mapper.Map<DetalleVentaProductoDto>(crearDetalleVenta));
                }
                else
                {
                    return BadRequest("Error al guardar los datos ");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<DetalleVentaProductoDto>> Put(int ventaId, int id,
            DetalleVentaProductoDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Editar: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var editar = _detalleVentaProductoRepository
                    .ObtenerDetalleVenta(id, ventaId, false);


                var mapperDetalleVenta = Mapper.Map(dto, editar);

                var editado = await _detalleVentaProductoRepository.ActualizarAsync(mapperDetalleVenta);

                return Mapper.Map<DetalleVentaProductoDto>(editado);
            }
            catch (Exception ex)
            {
                _logger.LogInformation("editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
    }
}