using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.ClienteRepository;
using DigitalWare.DataAccess.Dto.Usuario;
using DigitalWare.DataAccess.Entity.Usuario;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json", "application/xml")]
    [ApiController]
    public class ClienteDWController : ControllerBase
    {
        private readonly ILogger<ClienteDWController> _logger;
        private readonly IClienteRepository _clienteRepository;
        private readonly LinkGenerator _linkGenerator;

        public ClienteDWController(ILogger<ClienteDWController> logger,
            IClienteRepository clienteRepository, LinkGenerator linkGenerator)
        {
            _logger = logger;
            _clienteRepository = clienteRepository;
            _linkGenerator = linkGenerator;
        }

        /// <summary>
        ///  obtener cliente por id 
        /// </summary>
        /// <param name="id"> identificacion </param>
        /// <returns>ClienteDto</returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ClienteDto>> Get(int id)
        {
            try
            {
                var cliente =await _clienteRepository.ObtenerPor(identificacion:id) ?? throw new ArgumentNullException($"_No hay resultados disponibles para el id: {id}");
           
                return Mapper.Map<ClienteDto>(cliente);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }

        }
        /// <summary>
        /// obtiene lista de clientes 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<ClienteListarDto>> Get()
        {
            try
            {
                return Mapper.Map<List<ClienteListarDto>>( _clienteRepository.ObtenerLista());
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }

        }
        
        [HttpPost]
        public async Task<ActionResult<Cliente>> Post(ClienteDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var location = _linkGenerator.GetPathByAction("Get",
                    "ClienteDw",
                    new { id = dto.Identificacion });

                if (string.IsNullOrWhiteSpace(location))
                {
                    return BadRequest("Could not use current ");
                }
                
                var clienteNuevo = Mapper.Map<Cliente>(dto);
                clienteNuevo.FechaNacimiento = Convert.ToDateTime(dto.FechaNacimiento);
                Cliente clienteCreado = await _clienteRepository.CrearAsync(clienteNuevo);
                
                return Created($"/api/clienteDw/{clienteCreado.Identificacion}", Mapper.Map<ClienteDto>(clienteCreado));
 
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Crear: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }

        [HttpPut("{identificacion:int}")]
        public async Task<ActionResult<ClienteDto>> Put(int identificacion, ClienteDto dto)
        {
            try
            {
                Cliente cliente = await _clienteRepository.ObtenerPor(identificacion);
                if (cliente == null)
                    return NotFound($"No hay resultados disponibles para el id {identificacion}");

                var clienteMapp = Mapper.Map(dto, cliente);
                
                _clienteRepository.Actualizar(clienteMapp);

                return Mapper.Map<ClienteDto>(cliente);
            }
            catch (Exception )
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");

            }
        }
        
        
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Borrar(int id)
        {
            if (id < 1)
            {
                _logger.LogError("Borrar: " + ModelState);
                return NotFound($"El id no puede ser 0 o mernor {id}");
            }

            try
            {
                if (!await _clienteRepository.ExistePor(id))
                    return NotFound($"No hay resultados disponibles para el id {id}");


                _clienteRepository.Eliminar(_clienteRepository.Obtener(id));

                return Ok($"Proceso termino exitosamente {id}");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Borrar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");
            }
        }
    }
}