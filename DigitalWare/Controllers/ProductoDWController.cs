using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using DigitalWare.DataAccess.Dto.ProductoDtos;
using DigitalWare.DataAccess.Entity.Facturacion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace DigitalWare.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json", "application/xml")]
    [ApiController]
    public class ProductoDWController : ControllerBase
    {
        private readonly ILogger<ProductoDWController> _logger;
        private readonly IProductoRepository _productoRepository;
        private readonly LinkGenerator _linkGenerator;

        public ProductoDWController(ILogger<ProductoDWController> logger,
            IProductoRepository productoRepository, LinkGenerator linkGenerator)
        {
            _logger = logger;
            _productoRepository = productoRepository;
            _linkGenerator = linkGenerator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serie"></param>
        /// <returns></returns>
        [HttpGet("{serie}")]
        public async Task<ActionResult<ProductoDto>> Get(string serie)
        {
            try
            {
                var producto = await _productoRepository.ObtenerPor(serie: serie) ??
                               throw new ArgumentNullException($"_No hay resultados disponibles para el id: {serie}");

                return producto;
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }

        [HttpGet]
        public ActionResult<List<ProductoListarDto>> Get()
        {
            var listaProductos = _productoRepository.ObtenerListaConTotalCantidades();
            return Mapper.Map<List<ProductoListarDto>>(listaProductos);
        }
        
        [HttpGet("{id:int}")]
        public ActionResult<ProductoDto> Get(int id)
        {
            try
            {
                var producto = _productoRepository.Obtener(id) ??
                               throw new ArgumentNullException($"_No hay resultados disponibles para el id: {id}");

                return  Mapper.Map<ProductoDto>(producto);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }

        [HttpPost]
        public async Task<ActionResult<ProductoCrearDto>> Post(ProductoCrearDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var location = _linkGenerator.GetPathByAction("Get",
                    "ProductoDW",
                    new {id = dto.Id});

                if (string.IsNullOrWhiteSpace(location))
                {
                    return BadRequest("Could not use current ");
                }

                Producto producto = Mapper.Map<Producto>(dto);

                var crearProducto = await _productoRepository.CrearAsync(producto);

                return Created($"/api/productoDw/{crearProducto.Id}",
                    Mapper.Map<ProductoCrearDto>(crearProducto));
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<ProductoEditarDto>> Put(int id, ProductoEditarDto dto)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Crear: " + ModelState);
                return new UnprocessableEntityObjectResult(ModelState);
            }

            try
            {
                var antiguoProducto = _productoRepository.Obtener(id);

                if (antiguoProducto == null) return BadRequest($"No hay resultados disponibles para el id: {id}");
               
                var editar = Mapper.Map(dto, antiguoProducto); 

                var editado =await _productoRepository.ActualizarAsync(editar);
                
                
                return Ok(Mapper.Map<ProductoEditarDto>(editado));
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editar: " + ex);
                return StatusCode(StatusCodes.Status500InternalServerError, $"{ex}");

            }
        }
        
    }
}