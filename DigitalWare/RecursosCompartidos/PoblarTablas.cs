using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DigitalWare.DataAccess.DataContext;
using DigitalWare.DataAccess.Entity.Facturacion;
using DigitalWare.DataAccess.Entity.Usuario;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace DigitalWare.RecursosCompartidos
{
    public static class PoblarTablas
    {
        public static async Task Poblar(IServiceProvider serviceProvider)
        {
            var dataBase = serviceProvider.GetRequiredService<DigitalWareContext>();

            if (!dataBase.Clientes.Any())
            {
                var listadoClientes = GetData<Cliente>("ListadoClientes.json");
                await dataBase.AddRangeAsync(listadoClientes);
            }

            if (!dataBase.Productos.Any())
            {
                var listadoProductos = GetData<Producto>("ListadoProductos.json");
                await dataBase.AddRangeAsync(listadoProductos);
            }

            if (!dataBase.Ventas.Any())
            {
                var listadoVenta = GetData<Venta>("ListadoVentas.json");
                await dataBase.AddRangeAsync(listadoVenta);

            }

            await dataBase.SaveChangesAsync();
        }


        private static IEnumerable<TData> GetData<TData>(string filename) where TData : class
        {
            var pathToSeedData = Path.Combine(Directory.GetCurrentDirectory(), "RecursosCompartidos", "SeedData", filename);
            var dataSet = File.ReadAllText(pathToSeedData);

            var seedData = JsonConvert.DeserializeObject<IEnumerable<TData>>(dataSet);
            return seedData;

        }
    }
}