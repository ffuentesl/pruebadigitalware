using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DigitalWare.DataAccess.DataContext;
using DigitalWare.DataAccess.DataContext.Interface;
using DigitalWare.DataAccess.DataContext.Repository.ClienteRepository;
using DigitalWare.DataAccess.DataContext.Repository.FacturacionRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AutoMapper;
using DigitalWare.DataAccess.Dto.PaginarData;
using DigitalWare.RecursosCompartidos;
using Microsoft.AspNetCore.Mvc.Versioning;


namespace DigitalWare
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            
            //services.AddControllers();
            services.AddMvc(setupAction: setupAction =>
            {
                

                setupAction.ReturnHttpNotAcceptable = true;

                setupAction.OutputFormatters.Add(item: new XmlSerializerOutputFormatter());

                var jsonOutputFormatter = setupAction.OutputFormatters
                    .OfType<SystemTextJsonOutputFormatter>().FirstOrDefault();

                if (jsonOutputFormatter != null)
                {
                    
                    if (jsonOutputFormatter.SupportedMediaTypes.Contains(item: "text/json"))
                    {
                        jsonOutputFormatter.SupportedMediaTypes.Remove(item: "text/json");
                    }
                }
            }).SetCompatibilityVersion(version: CompatibilityVersion.Version_3_0);
            
            
            services.AddTransient<IDigitalWareContext, DigitalWareContext>();
             
            services.AddTransient<IClienteRepository, ClienteRepository>();
            services.AddTransient<IDetalleVentaProductoRepository, DetalleVentaProductoRepository>();
            services.AddTransient<IExistenciaProductoRepository, ExistenciaProductoRepository>();
            services.AddTransient<IProductoRepository, ProductoRepository>();
            services.AddTransient<IVentaRepository, VentaRepository>();
 


            services.AddDbContext<DigitalWareContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("PruebaTecnicaConection")));

            
            _ = services.AddAutoMapper();
            
            Mapper.Initialize(config: cfg => AutoMapperConfig.RegisterMappings(cfg: cfg));

       
                
            services.AddSwaggerGen(setupAction: setupAction =>
            {
                setupAction.SwaggerDoc(
                    name: "DWOpenAPISpecification",
                    info: new Microsoft.OpenApi.Models.OpenApiInfo()
                    {
                        Title = "Prueba DigitalWare API",
                        Version = "1",
                        Description = "Documentacion de la api",
                    });
                var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentsFullPath = Path.Combine(path1: AppContext.BaseDirectory, path2: xmlCommentsFile);

                setupAction.IncludeXmlComments(filePath: xmlCommentsFullPath);
                //SOSAmbiental.Api.xml
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            DigitalWareContext context,IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed((host) => true)
                .AllowCredentials()
            ); 
            
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseSwagger();
            app.UseSwaggerUI(setupAction=> {
                setupAction.SwaggerEndpoint(
                    "/swagger/DWOpenAPISpecification/swagger.json",
                    "Prueba DigitalWare API"
                );
            });


            app.UseAuthorization(); 

            /*app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });*/
            app.UseEndpoints(endpoints =>{ endpoints.MapControllers(); });
            
            context.Database.EnsureCreated();
            PoblarTablas.Poblar(serviceProvider).Wait();

        }
       
    }
}